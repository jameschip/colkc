#Colkc

Colck is a word clock for the linux terminal that outputs the time as random anagrams.

The anagrams update each second.

It is barely readable, not useful but functionally complete.

# Build

```
gcc -lncurses colck.c -o colck
```