#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <ncurses.h>
#include <unistd.h>

WINDOW *draw_win;

char *itowd( int i ) {
    char *single[] = {"", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
    char *teens[] = {"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
    char *tens[] = {"twenty ", "thirty ", "fourty ", "fifty ", "sixty " };

    if ( (i / 10) == 0 ) return single[i];

    if ( (i / 10) == 1 ) return teens[i-10];

    if ( (i / 10) > 1 ){
        char tmp[50];
        strcpy(tmp, tens[ (i / 10) - 2 ]);
        strcat( tmp, single[i % 10] );
        return strdup( tmp );
    }

    return "";
}

void shuffle(char *array, size_t n)
{
    if (n > 1) 
    {
        size_t i;
        for (i = 0; i < n - 1; i++) 
        {
          size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
          int t = array[j];
          array[j] = array[i];
          array[i] = t;
        }
    }
}

WINDOW *create_newwin(int height, int width, int starty, int startx)
{	
	WINDOW *local_win;
	local_win = newwin(height, width, starty, startx);
	return local_win;
}


int main( int argc, char * argv ) {

    int row, col = -1;
    bool draw = true;
    initscr();
    noecho();
    cbreak();
    getmaxyx( stdscr, row, col );
    curs_set(0);
    refresh();
    clear();
    draw_win = create_newwin( row, col, 0, 0 );


    time_t rawtime;
    struct tm * timeinfo;
    char t[200];
    while(1) {
        time(&rawtime);
        timeinfo = localtime(&rawtime);
        wclear(draw_win);
        wmove(draw_win, (row/2 - 1), 3);
        sprintf(t, "%s", itowd(timeinfo->tm_hour) );
        shuffle( t, strlen(t));
        wprintw(draw_win, "%s hours", t );
        wmove(draw_win, row/2, 3);
        sprintf(t, "%s", itowd(timeinfo->tm_min) );
        shuffle( t, strlen(t));
        wprintw(draw_win, "%s minutes", t );
        wmove(draw_win, (row/2 + 1), 3);
        sprintf(t, "%s", itowd(timeinfo->tm_sec) );
        shuffle( t, strlen(t));
        wprintw(draw_win, "%s seconds", t );
        wrefresh(draw_win);
        sleep(1);
    }

    delwin(draw_win);
 
    return 0;
}

